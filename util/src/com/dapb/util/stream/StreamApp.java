package com.dapb.util.stream;

import java.util.ArrayList;
import java.util.List;

public class StreamApp {

	private List<String> lstDatos;
	private List<String> lstNumeros;

	public StreamApp() {
		lstDatos = new ArrayList<>();
		lstDatos.add("Diego");
		lstDatos.add("Alejandro");
		lstDatos.add("Panqueva");
		lstDatos.add("Benitez");

		lstNumeros = new ArrayList<>();
		lstNumeros.add("1");
		lstNumeros.add("2");
	}

	public void filtrar() {
		lstDatos.stream().filter(e -> e.startsWith("A")).forEach(System.out::println);
		;
	}

	public void ordenar() {

		// es de forma ascendente
		lstDatos.stream().sorted().forEach(System.out::println);
		// forma descendente
		lstDatos.stream().sorted((x, y) -> y.compareToIgnoreCase(x)).forEach(System.out::println);
	}

	public void transformar() {
		// con el stream podemos ingresar a metodos de transformacion como el map
		// lstDatos.stream().map(String::toUpperCase).forEach(System.out::println);

		// Otro ejemplo
		// Manera imperativa
		/*for(String elemento : lstNumeros) {
			int num = Integer.parseInt(elemento) +1;
			System.out.println(num);
		}*/
		lstNumeros.stream().map(x -> Integer.parseInt(x) + 1).forEach(System.out::println);
	}

	public void limitar() {
		//limitar con lambda la entrega a un n�mero entero
		lstDatos.stream().limit(2).forEach(System.out::println);
	}

	public void contar() {
		long cuantos = lstDatos.stream().count();
		System.out.println(cuantos);
	}

}
