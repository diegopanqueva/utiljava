package com.dapb.util.metodo.referencia;

@FunctionalInterface
public interface IPersona {
	
	PersonaModel crear(int id, String nombre);

}
