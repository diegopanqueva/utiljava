package com.dapb.util.metodo.referencia;

import java.util.Arrays;
import java.util.Comparator;

public class MeRef {

	public void operar() {
		// referencia a trav�s de un lambda
//		IOperacion operacion = ()->MeRef.rMetodoStatico();
//		operacion.saludar();
		// referencia a trav�s de ::
		// No envia parametros por el concepto de currificaci�n
		IOperacion operacion = MeRef::rMetodoStatico;
		operacion.saludar();
	}

	public void rConstructor() {

		//Accedo de forma tradicional a una implementaci�n de tipo
		//interface funcional
		/*IPersona iper = new IPersona() {

			@Override
			public PersonaModel crear(int id, String nombre) {
				return new PersonaModel(id, nombre);
			}
		};
		iper.crear(1, "Diego Panqueva");*/
		
		//Accedo a una interface funcional por medio de una referencia lambda
		/*IPersona iper2 = (x,y)->(new PersonaModel(x,y));
		PersonaModel per1 = iper2.crear(1, "Diego Panqueva");
		*/
		//Acceso a una interface funcional por medio de la referencia :: y constructor
		IPersona iper3 = PersonaModel::new;
		PersonaModel per2 = iper3.crear(1, "Diego Panqueva");
		
		System.out.println(per2.getId() + " - " + per2.getNombre());
		
	}

	public void rObjetoParticular() {
		System.out.println("Metodo referido de una instancia de clase particular");
	}

	public void rObjetoArbitrario() {
		String[] nombres = { "Diego", "Alejandro", "Panqueva", "Benitez" };
		/*
		 * Arrays.sort(nombres,new Comparator<String>() {
		 * 
		 * @Override public int compare(String o1, String o2) { return
		 * o1.compareToIgnoreCase(o2); } });
		 */

		// reducci�n lambda
		// Arrays.sort(nombres,(s1,s2)->s1.compareToIgnoreCase(s2));
		// reducci�n a referencia metodo
		Arrays.sort(nombres, String::compareToIgnoreCase);
		System.out.println(Arrays.toString(nombres));

	}

	public static void rMetodoStatico() {
		System.out.println("Metodo referido static");
	}
}
