package com.dapb.util.interfaz.funcional;

public class OperacionFuncionalImpl {

	public double operar(double x, double y) {
		IOperacionFuncional operacion = (n1, n2) -> n1 + n2;
		double res = operacion.calcular(x, y);
		System.out.println(res);
		return res;
	}

}
