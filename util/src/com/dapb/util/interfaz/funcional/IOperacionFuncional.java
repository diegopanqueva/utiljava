package com.dapb.util.interfaz.funcional;

//Anotación que hace referencia a que mi interface es una interface funcional
@FunctionalInterface
public interface IOperacionFuncional {
	
	/*
	 * Una interface funcional es aquella que define una unica operacion
	 * o un unico metodo
	 * */

	double calcular(double n1, double n2);
	
}
