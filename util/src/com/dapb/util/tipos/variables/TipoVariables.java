package com.dapb.util.tipos.variables;

public class TipoVariables {

	//variables, son aquellas que cambian en cualquier parte del c�digo
	//la edad cambia cada a�o
	private int edad = 29;
	
	//una constante es aquella que no cambia que se mantiene siempre
	//despues de la tarjeta de identidad y obtener la c�dula este n�mero no cambia
	private final int cedula;
	
	//variable static o estatica, son todos aquellos que en una instancia NEW no son
	//parte del nuevo objeto sino que siempre le van a pertenecer a la clase, esto
	//quiere decir que si yo tengo una variable static esta en la nueva instancia ser� unica
	//para todos los objetos que se creen apartir de la clase
	public static int edadStatic = 200;
	
	//Constructor
	//este se pone para darle valor a los variables que hacen parte de la clase
	public TipoVariables(int cedula,int edad) {
		this.edad = edad;
		this.cedula = cedula;
	}
	
	
	public void setEdad(int edad){
		this.edad = edad;
	}
	
	public int getEdad(){
		return this.edad;
	}
	
}
