package com.dapb.util.foreach.remove;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MetodosReccorrido {

	private List<String> lstDatos;

	public void llenarLista() {
		lstDatos = new ArrayList<>();
		lstDatos.add("Diego");
		lstDatos.add("Alejandro");
		lstDatos.add("Panqueva");
		lstDatos.add("Benitez");

	}

	public void usarForEach() {
		// forma tradicional en java 7
		/*
		 * for(String elemento : lstDatos) { System.out.println(elemento); }
		 */

		// forma actual en java 8 con lambdas
		// lstDatos.forEach(e->System.out.println(e));
		// forma actual en java 8 con lambda y referencia a metodos
		lstDatos.forEach(System.out::println);
	}

	public void removeIf() {
		//forma de eliminar un dato de una lista en ejecución
		Iterator<String> it = lstDatos.iterator();
		while(it.hasNext()) {
			if(it.next().equalsIgnoreCase("Panqueva")) {
				it.remove();
			}
		}
		
		lstDatos.removeIf(e->e.equalsIgnoreCase("Panqueva"));
	}
	
	public void usarSort() {
		lstDatos.sort((x,y)->x.compareToIgnoreCase(y));
	}

}
