package com.dapb.util.polimorfismo;

public class Caballo extends Animal{
	
	private String raza;
	
	public Caballo(String nombre, String raza, int edad) {
		super(nombre,raza,edad);
		this.raza = raza;
	}
	

	/**
	 * @return the raza
	 */
	public String getRaza() {
		return raza;
	}



	/**
	 * @param raza the raza to set
	 */
	public void setRaza(String raza) {
		this.raza = raza;
	}



	@Override
	public void alimentarse() {
		System.out.println("Me alimento de hierbas finas");
	}

}
