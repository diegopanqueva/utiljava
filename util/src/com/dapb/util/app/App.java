package com.dapb.util.app;

public class App {

	public static void main(String[] args) {
		System.out.println("Ejercicios DAPB!");
		Ejercicios ejercicio = Ejercicios.getInstancia();
		// ejercicio.ejercicioLambda();
		// ejercicio.ejercicioMetodosXDefecto();
		// ejercicio.ejerciciosInterfazFuncional();
		// ejercicio.ejercicioReferenciaMetodos();
		// ejercicio.ejercicioRecorridoLambda();
		//ejercicio.ejercicioStream();
		//ejercicio.ejercicioPolimorfismo();
		//ejercicio.ejercicioEncapsulamiento();
		//ejercicio.ejercicioPatronSingleton();
		//ejercicio.ejercicioPatronFabrica();
		ejercicio.ejercicioTipoVaribles();
	}

}
