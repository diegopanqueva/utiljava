package com.dapb.util.app;

import com.dapb.util.encapsulamiento.Carro;
import com.dapb.util.encapsulamiento.p1.Tren;
import com.dapb.util.foreach.remove.MetodosReccorrido;
import com.dapb.util.interfaz.funcional.OperacionFuncionalImpl;
import com.dapb.util.lambda.Lambda;
import com.dapb.util.metodo.referencia.IOperacion;
import com.dapb.util.metodo.referencia.MeRef;
import com.dapb.util.metodos.defecto.PersonaImpl;
import com.dapb.util.patrones.fabrica.ConexionFabrica;
import com.dapb.util.patrones.fabrica.inter.IConexion;
import com.dapb.util.patrones.singleton.SinSingleton;
import com.dapb.util.patrones.singleton.Singleton;
import com.dapb.util.polimorfismo.Animal;
import com.dapb.util.polimorfismo.Caballo;
import com.dapb.util.polimorfismo.Gato;
import com.dapb.util.stream.StreamApp;
import com.dapb.util.tipos.variables.TipoVariables;

public class Ejercicios {

	private static Ejercicios instancia;

	public static Ejercicios getInstancia() {
		if (instancia == null) {
			instancia = new Ejercicios();
		}
		return instancia;
	}

	private Ejercicios() {
	}

	/*
	 * Metodo de ejercicios lambda incluidos en jdk 8
	 */
	public void ejercicioLambda() {
		Lambda lambda = new Lambda();
		lambda.ordenar();
		lambda.calcular();
		lambda.variablesLocales();
		lambda.variablesStatic();
	}

	public void ejercicioMetodosXDefecto() {
		// metodos por defecto
		PersonaImpl persona = new PersonaImpl();
		persona.caminar();
		persona.hablar();
	}

	public void ejerciciosInterfazFuncional() {
		// interfaces funcionales
		OperacionFuncionalImpl operacion = new OperacionFuncionalImpl();
		operacion.operar(12, 32);
	}

	public void ejercicioReferenciaMetodos() {
		// Referencia a metodos
		MeRef mRef = new MeRef();
		mRef.operar();
		mRef.rObjetoArbitrario();
		// Interface funcional, se le ha implementado un metodo de instancia
		// del objeto mRef para poder acceder a su unico metodo saludar

		IOperacion op = mRef::rObjetoParticular;
		op.saludar();

		// referencia a un constructor
		mRef.rConstructor();
	}

	public void ejercicioRecorridoLambda() {
		MetodosReccorrido reccorrido = new MetodosReccorrido();
		reccorrido.llenarLista();
		// reccorrido.removeIf();
		reccorrido.usarSort();
		reccorrido.usarForEach();
	}

	public void ejercicioStream() {
		// api stream
		StreamApp app = new StreamApp();
		app.filtrar();
		app.ordenar();
		app.transformar();
		app.limitar();
		app.contar();
	}

	public void ejercicioPolimorfismo() {
		// Polimorfismo
		// Son las multiples formas que puede tomar un objeto de acuerdo a sus
		// clases hijas

		Animal c = new Caballo("Maximus", "Pony", 10);
		c.alimentarse();

		Animal g = new Gato();
		g.alimentarse();
	}

	public void ejercicioEncapsulamiento() {
		// se encarga de ocular nuestros atributos y/o metodos
		// para ser accedidos desde la misma clase
		
		//publicos: pueden ser accedidos por todas las clases del
		//aplicativo
		//private: deben ser accedidos a través de metodos de la
		//misma clase.
		//protected: son aquellas clases o atributos que pueden ser
		//accedidos desde su mismo paquete y desde la clase que hereda
		//independientemente si esta se encuentra en el mismo paquete
		Carro miCarro = new Carro();
		miCarro.carroTipoLlanta();
		
		Tren miTren = new Tren();
		miTren.trenTipoLlanta();
		
	}

	public void ejercicioPatronSingleton() {
		// Patrones: solucion estandarizada para agrupar el codigo o componentes
		// para que sean sostenibles y reutilizables
		
		//Creacionaes: Inicialización y configuracion de objetos
		//estructurales: Separan la interfaz de la implementación, estructuras complejas
		//Comportamientos: Describe objetos y clases implicadas y como se comunican.
		
		//1. singleton
		Singleton singleton =Singleton.getInstancia();
		System.out.println("Mi instancia es: " +singleton);
		SinSingleton sinSin = new SinSingleton();
		System.out.println("Mi instancia sin singleton es: " +sinSin);
		Singleton singleton1 =Singleton.getInstancia();
		System.out.println("Mi nueva instancia es: " +singleton1);
		SinSingleton sinSin1 = new SinSingleton();
		System.out.println("Mi nueva instancia sin singleton es: " +sinSin1);
	}

	public void ejercicioPatronFabrica() {
		// Objetivo es devolver instancia por medio de algun identificador
		// necesidad, la fabrica es responsable de devolver la instancia
		ConexionFabrica fabrica = new ConexionFabrica();
		IConexion cx = fabrica.getConexion("MYSQL");
		cx.conectar();
		cx.desconectar();
		
		IConexion cx1 = fabrica.getConexion("ORACLE");
		cx1.conectar();
		cx1.desconectar();
		
		IConexion cx3 = fabrica.getConexion("H2");
		cx3.conectar();
		cx3.desconectar();
	}
	
	public void ejercicioTipoVaribles(){
		
		//este ejemplo pertenece a la variable static
		TipoVariables obj1 = new TipoVariables(1013, 29);
		TipoVariables obj2 = new TipoVariables(1013, 30);
		obj1.setEdad(25);
		//edad cambiara de 29 a 25
		//imprime la edad correspondiente del objeto creado
		System.out.println("La edad del OBJ1:"+obj1.getEdad());
		System.out.println("La edad del OBJ2:"+obj2.getEdad());
		
		//ahora trabajaremos con la static
		TipoVariables objStatic1 = new TipoVariables(1013, 29);
		TipoVariables objStatic2 = new TipoVariables(1013, 30);
		objStatic1.edadStatic = 1000;
		objStatic2.edadStatic = 100;
		
		System.out.println("La edad del OBJSTATIC1:"+objStatic1.edadStatic);
		System.out.println("La edad del OBJSTATIC2:"+objStatic2.edadStatic);
		//el valor edadStatic que cambia de ultimo es 100, eso quiere decir que para
		//todos los objetos (objStatic1,objStatic2) el valor es 100. 

	}
}

