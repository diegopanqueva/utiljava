package com.dapb.util.patrones.fabrica.inter.impl;

import com.dapb.util.patrones.fabrica.inter.IConexion;

public class ConexionOracle implements IConexion {

	private String host;
	private String puerto;
	private String usuario;
	private String clave;

	public ConexionOracle() {
		this.host = "localhost";
		this.puerto = "1531";
		this.usuario = "SID";
		this.clave = "1234";
	}

	@Override
	public void conectar() {
		System.out.println("Conectando a bd ORACLE " + host + ":" + puerto + " " + usuario + " " + clave);

	}

	@Override
	public void desconectar() {
		System.out.println("Desconectando a bd ORACLE");

	}

}
