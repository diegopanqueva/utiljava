package com.dapb.util.patrones.fabrica.inter.impl;

import com.dapb.util.patrones.fabrica.inter.IConexion;

public class ConexionMysql implements IConexion{

	private String host;
	private String puerto;
	private String usuario;
	private String clave;
	
	public ConexionMysql() {
		this.host="localhost";
		this.puerto="3306";
		this.usuario="root";
		this.clave="1234";
	}
	
	@Override
	public void conectar() {
		System.out.println("Conectando a bd mysql " + host +":"+puerto+" " +usuario+" " +clave);
		
	}

	@Override
	public void desconectar() {
		System.out.println("Desconectando a bd mysql");
		
	}

}
