package com.dapb.util.patrones.fabrica.inter;

public interface IConexion {

	void conectar();
	
	void desconectar();
}
