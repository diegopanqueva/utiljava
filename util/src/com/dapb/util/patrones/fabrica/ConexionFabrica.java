package com.dapb.util.patrones.fabrica;

import com.dapb.util.patrones.fabrica.inter.IConexion;
import com.dapb.util.patrones.fabrica.inter.impl.ConexionMysql;
import com.dapb.util.patrones.fabrica.inter.impl.ConexionOracle;
import com.dapb.util.patrones.fabrica.inter.impl.ConexionVacia;

public class ConexionFabrica {

	
	public IConexion getConexion(String motor) {
		if(motor == null) {
			return new ConexionVacia();
		}
		if(motor.equalsIgnoreCase("MYSQL")) {
			return new ConexionMysql();
		}
		if(motor.equalsIgnoreCase("ORACLE")) {
			return new ConexionOracle();
		}
		
		return new ConexionVacia();
	}
}
