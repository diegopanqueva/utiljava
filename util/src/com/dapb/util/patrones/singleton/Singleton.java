package com.dapb.util.patrones.singleton;

public class Singleton {

	/*
	 * patron de dise�o que me ayuda a tener un control de instancias generadas en
	 * mi aplicativo, esto con el fin de no llenar mi memeria de objetos con
	 * instancias.
	 */

	// Se declara una variable del mismo tipo de la clase de tipo static
	// la cual no cambiara ni modificara su valor
	private static Singleton instancia;

	/*
	 * Metodo para obtener una unica instancia de mi clase devolvera el mismo tipo
	 * de la clase como instancia
	 */
	public static Singleton getInstancia() {
		if (instancia == null) {
			instancia = new Singleton();
		}
		return instancia;
	}

	// su constuctor es privado
	private Singleton() {
		// TODO Auto-generated constructor stub
	}

}
