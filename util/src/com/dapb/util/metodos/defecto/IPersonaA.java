package com.dapb.util.metodos.defecto;

public interface IPersonaA {

	public void caminar();
	
	//escribiendo keywork, ambito y el tipo de metodo
	default public void hablar() {
		System.out.println("Estoy hablando");
	}
}
