package com.dapb.util.metodos.defecto;

public class PersonaImpl implements IPersonaA,IPersonaB{

	@Override
	public void caminar() {
		System.out.println("Hola dapb voy caminando");
		
	}
	
	/*
	 * Si tengo metodos llamados igual y neesito instanciar uno en especial
	 * puedo hacer referencia de las super clases
	 * */
	@Override
	public void hablar() {
		IPersonaB.super.hablar();
		
	}

}
