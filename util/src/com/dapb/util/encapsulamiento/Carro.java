package com.dapb.util.encapsulamiento;

public class Carro extends TransporteModel{
	
	public void carroTipoLlanta() {
		TransporteModel tm = new TransporteModel();
		// desde esta clase si puedo acceder al metodo y variable protected
		// debido a que esta clase hereda de la clase que la contiene
		tipoRuedaLlanta("Goma - Michelin");
		tm.setNumeroPasajeros(4);
		tm.setPlaca("ASD098");
		tm.setTipo("Carro");
		
		
		System.out.println(
				"Mi " + tm.getTipo() + " puede llevar " 
					  + tm.getNumeroPasajeros() 
					  + " y su placa es " 
					  + tm.getPlaca() 
					  + " se a�ade el tipo de rueda " 
					  + tipoLlanta);
		
	}

}
