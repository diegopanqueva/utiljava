package com.dapb.util.encapsulamiento.p1;

import com.dapb.util.encapsulamiento.TransporteModel;

public class Tren {

	public void trenTipoLlanta() {
		TransporteModel tm = new TransporteModel();
		// no puedo acceder al metodo ni a la variable protected
		// ya que no pertenece al mismo paquete
		// tm.tipoRuedaLlanta
		tm.setNumeroPasajeros(100);
		tm.setPlaca("avv04e");
		tm.setTipo("Tren");

		System.out.println(
				"Mi " + tm.getTipo() + " puede llevar " 
					  + tm.getNumeroPasajeros() 
					  + " y su placa es " 
					  + tm.getPlaca());
	}
}
