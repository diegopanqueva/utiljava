package com.dapb.util.encapsulamiento;

public class TransporteModel {

	
	private String tipo;
	private int numeroPasajeros;
	private String placa;
	
	protected String tipoLlanta;
	
	protected void tipoRuedaLlanta(String llanta) {
		tipoLlanta = llanta;
		System.out.println("El tipo de llanta es " + tipoLlanta + " ,accedido desde el mismo paquete");
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the numeroPasajeros
	 */
	public int getNumeroPasajeros() {
		return numeroPasajeros;
	}

	/**
	 * @param numeroPasajeros the numeroPasajeros to set
	 */
	public void setNumeroPasajeros(int numeroPasajeros) {
		this.numeroPasajeros = numeroPasajeros;
	}

	/**
	 * @return the placa
	 */
	public String getPlaca() {
		return placa;
	}

	/**
	 * @param placa the placa to set
	 */
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	
	
	
}
