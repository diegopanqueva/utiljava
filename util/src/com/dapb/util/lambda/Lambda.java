package com.dapb.util.lambda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Lambda {
	
	//variablesStatic
	private static double a1;
	private double a2;

	/*
	 * Como ordeenar una lista de datos utilizando lambdas ejemplo con java 7 y
	 * traducción a java 8
	 */
	public void ordenar() {
		List<String> lstDatos = new ArrayList<>();
		lstDatos.add("Diego");
		lstDatos.add("Alejandro");
		lstDatos.add("Panqueva");
		lstDatos.add("Benitez");
		/*
		 * //Java < 7 Collections.sort(lstDatos, new Comparator<String>() {
		 * 
		 * @Override public int compare(String o1, String o2) { return o1.compareTo(o2);
		 * } });
		 */
		// Expresion lambda
		Collections.sort(lstDatos, (String p1, String p2) -> p1.compareTo(p2));
		for (String elemento : lstDatos) {
			System.out.println(elemento);
		}
	}

	/*
	 * Calular un valor por medio de lambdas y una clase Las interfaces no se pueden
	 * instanciar a menos que se sobrescriba su o sus metodos
	 */
	public void calcular() {
		// metodo tradicional
		// Sobres escribir el metodo para poder instanciar el código
		/*
		 * IOperacion operacion = new IOperacion() {
		 * 
		 * @Override public double promedio(double n1, double n2) { return (n1+n2)/2; }
		 * };
		 */

		// metodo lambda
		// () implementación
		IOperacion operacion = (double x, double y) -> (x + y) / 2;

		System.out.println(operacion.promedio(10, 30));
	}

	/*
	 * Metodo para verificar el comportamiento de los lambdas con variables locales
	 */
	public double variablesLocales() {
		// definición de variable
		final double n3 = 3;
		IOperacion operacion = (x, y) -> {
			// no puedo utilizar n3 si esta no esta con definicion final
			// final me dice que no puedo asignar su valor, siempre sera
			// el valor que anteriormente se ha asignado.
			// n3 = 2.0; //al ser final no puedo asignar un valor, pero si
			// puedo utilizarlo
			return x + y + n3;
		};
		double yx = operacion.promedio(15, 34);
		System.out.println(yx);
		return yx;
	}

	/*
	 * Metodo para verificar comportamiento de lambda con variables staticas
	 * globales
	 * */
	public double variablesStatic() {
		//Puedo asignar un valor al atributo 1 a1, ya que esta definido globalmente y de 
		//manera statica.
		
		//Puedo asignarle valor a mi variable local atributo 2 a2, ya que el lambda me 
		//permite utilizar estas variables siempre y cuando su declaracción se global
		IOperacion operacion = (x,y)->{
			a1= x+y;
			a2 = a1;
			return (a2);
		};
		double xy = operacion.promedio(12, 23);
		System.out.println(xy);
		return xy;
	}
}
